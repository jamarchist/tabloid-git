namespace Tabloid
{
    public static class TestStepExtensions
    {
        public static TTestStep WithOption<TTestStep>(this TTestStep step, string option, object value) where TTestStep : ITestStep
        {
            step.Options[option] = value;
            return step;
        }

        public static TTestStep WithOption<TTestStep>(this TTestStep step, string option) where TTestStep : ITestStep
        {
            step.Options[option] = true;
            return step;
        }

        public static bool HasOption(this ITestStep step, string option)
        {
            if (step.Options.ContainsKey(option))
            {
                return true;
            }

            return false;
        }
    }
}