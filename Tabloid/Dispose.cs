﻿using System;

namespace Tabloid
{
    public class Dispose : TestStep
    {
        private readonly Action<IScenarioTest> tearDown;

        public Dispose(Action<IScenarioTest> tearDown)
        {
            this.tearDown = tearDown;
        }

        public override void TearDown()
        {
            tearDown(Test);
        }
    }
}