﻿using System;

namespace Tabloid
{
    public class Init : TestStep
    {
        private readonly Action<IScenarioTest> setUp;

        public Init(Action<IScenarioTest> setUp)
        {
            this.setUp = setUp;
        }

        public override void SetUp()
        {
            setUp(Test);
        }
    }
}