using System;

namespace Tabloid
{
    public class TestStepFailedException : Exception
    {
        public TestStepFailedException(string failingFunction, ITestStep step, Exception innerException) :
            base(String.Format("The '{0}' portion of test step '{1}' failed. See inner exception for details.", failingFunction, step.GetType().Name), innerException)
        {            
        }
    }
}