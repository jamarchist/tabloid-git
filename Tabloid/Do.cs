﻿using System;

namespace Tabloid
{
    public class Do : TestStep
    {
        private readonly Action<IScenarioTest> execute;

        public Do(Action<IScenarioTest> execute)
        {
            this.execute = execute;
        }

        public override void Execute()
        {
            execute(Test);
        }
    }
}
