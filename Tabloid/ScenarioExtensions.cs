﻿namespace Tabloid
{
    public static class ScenarioExtensions
    {
        public static TResourceType ResourceAs<TResourceType>(this IScenarioTest test, string key)
        {
            return (TResourceType) test.Resources[key];
        }
    }
}
