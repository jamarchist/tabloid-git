using System.Collections.Generic;

namespace Tabloid
{
    public interface IScenarioTest
    {
        IDictionary<string, object> Resources { get; } 
    }
}