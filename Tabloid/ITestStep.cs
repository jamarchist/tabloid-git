using System.Collections.Generic;

namespace Tabloid
{
    public interface ITestStep
    {
        void BeforeSetUp();
        void SetUp();
        void AfterSetUp();

        void BeforeExecute();
        void Execute();
        void AfterExecute();

        void BeforeTearDown();
        void TearDown();
        void AfterTearDown();

        void BeforeAssert();
        //void Assert(); TODO: Figure this out
        void AfterAssert(); 

        IScenarioTest Test { get; set; }
        IDictionary<string, object> Options { get; set; }
    }
}