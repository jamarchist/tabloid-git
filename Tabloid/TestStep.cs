using System.Collections.Generic;

namespace Tabloid
{
    public class TestStep : ITestStep
    {
        public TestStep()
        {
            Options = new Dictionary<string, object>();
        }

        public virtual void BeforeSetUp() { }
        public virtual void SetUp() { }
        public virtual void AfterSetUp() { }

        public virtual void BeforeExecute() { }
        public virtual void Execute() { }
        public virtual void AfterExecute() { }

        public virtual void BeforeTearDown() { }
        public virtual void TearDown() { }
        public virtual void AfterTearDown() { }

        public virtual void BeforeAssert() { }
        //public virtual void Assert() { }
        public virtual void AfterAssert() { }

        public virtual IScenarioTest Test { get; set; }
        public IDictionary<string, object> Options { get; set; } 

        protected void Log(string stepMethod, string messageFormat, params object[] arguments)
        {
            var type = this.GetType().Name;
            System.Console.WriteLine("Test Step: [{1}] - {0} : {2}", type, stepMethod, System.String.Format(messageFormat, arguments));
        }
    }
}