using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Tabloid
{
    public class CompositeTestStep : TestStep
    {
        protected readonly List<ITestStep> testSteps;

        public CompositeTestStep(params ITestStep[] testSteps)
        {
            this.testSteps = testSteps.ToList();
        }

        public override void BeforeSetUp()
        {
            TryDo(s => s.BeforeSetUp());
        }

        public override void SetUp()
        {
            TryDo(s => s.SetUp());
        }

        public override void AfterSetUp()
        {
            TryDo(s => s.AfterSetUp());
        }

        public override void BeforeExecute()
        {
            TryDo(s => s.BeforeExecute());
        }

        public override void Execute()
        {
            TryDo(s => s.Execute());
        }

        public override void AfterExecute()
        {
            TryDo(s => s.AfterExecute());
        }

        public override void BeforeTearDown()
        {
            TryDo(s => s.BeforeTearDown());
        }

        public override void TearDown()
        {
            TryDo(s => s.TearDown());
        }

        public override void AfterTearDown()
        {
            TryDo(s => s.AfterTearDown());
        }

        public override void BeforeAssert()
        {
            TryDo(s => s.BeforeAssert());
        }

        public override void AfterAssert()
        {
            TryDo(s => s.AfterAssert());
        }

        public override IScenarioTest Test
        {
            get { return base.Test; }
            set { base.Test = value; testSteps.ForEach(s => s.Test = value); }
        }

        private void TryDo(Expression<Action<ITestStep>> stepMethod)
        {
            var methodName = (stepMethod.Body as MethodCallExpression).Method.Name;
            var dontExecute = String.Format("No{0}", methodName);
            var callStepMethodOn = stepMethod.Compile();

            foreach (var step in testSteps)
            {
                try
                {
                    if (!step.HasOption(dontExecute))
                    {
                        callStepMethodOn(step);                        
                    }
                }
                catch (Exception ex)
                {
                    throw new TestStepFailedException(methodName, step, ex);
                }
            }
        }
    }
}