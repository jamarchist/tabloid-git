using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Tabloid.NUnit.Tests
{
    [TestFixture]
    public class MyScenario : ScenarioTest
    {
        public override void DefineScenario()
        {
            AddStep(new Do(test =>
            {
                test.Resources["thing"] = new TestResource();
            }));
        }

        [Test]
        public void SomethingShouldHappen()
        {
            var thing = this.ResourceAs<TestResource>("thing");
            // Assert something about 'thing'
        }
    }


    public class TestResource : IDisposable
    {
        public void Dispose()
        {
            // 
        }
    }

    public class Thing { public string Id { get; set; } }

    public class ThingDatabase : IDisposable
    {
        private readonly IDictionary<string, Thing> things = new Dictionary<string, Thing>();

        public void AddThing(Thing thing)
        {
            things.Add(thing.Id, thing);
        }

        public Thing GetThing(string id)
        {
            return things[id];
        }

        public IList<Thing> GetAllThings()
        {
            return things.Values.ToList();
        }

        public void Dispose()
        {
            // This is just for demo purposes
            things.Clear();
        }
    }

    public class ThingAdder
    {
        public void Run(ThingDatabase db)
        {
            db.AddThing(new Thing { Id = "thing-2"});
            db.AddThing(new Thing { Id = "thing-3"});
        }
    }

    public class CreateEmptyDatabase : TestStep
    {
        public override void SetUp()
        {
            Test.Resources["db"] = new ThingDatabase();
        }

        public override void TearDown()
        {
            var db = Test.ResourceAs<ThingDatabase>("db");
            db.Dispose();
        }
    }

    public class AddDefaultData : TestStep
    {
        public override void SetUp()
        {
            var db = Test.ResourceAs<ThingDatabase>("db");
            db.AddThing(new Thing { Id = "thing-1"});
        }
    }

    public class RunExternalProcess : TestStep
    {
        public override void Execute()
        {
            // Run some process that talks to the database
            var db = Test.ResourceAs<ThingDatabase>("db");
            var adder = new ThingAdder();
            adder.Run(db);
        }
    }

    [TestFixture]
    public class when_adding_things_to_a_default_database : ScenarioTest
    {
        public override void DefineScenario()
        {
            AddStep(new CreateEmptyDatabase());
            AddStep(new AddDefaultData());
            AddStep(new RunExternalProcess());
        }

        [Test]
        public void there_should_be_exactly_3_things_in_the_database()
        {
            var db = this.ResourceAs<ThingDatabase>("db");
            Assert.AreEqual(3, db.GetAllThings().Count);
        }

        [TestCase("thing-2")]
        [TestCase("thing-3")]
        public void specific_things_should_exist(string thingId)
        {
            var db = this.ResourceAs<ThingDatabase>("db");
            Assert.IsNotNull(db.GetThing(thingId));
        }
    }
}