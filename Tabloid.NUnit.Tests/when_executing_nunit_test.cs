﻿using System;
using NUnit.Framework;

namespace Tabloid.NUnit.Tests
{
    [TestFixture]
    public class when_executing_nunit_test : ScenarioTest
    {
        public override void DefineScenario()
        {
            AddStep(new WriteToConsole());
        }

        [Test]
        public void everything_should_happen_in_the_correct_order()
        {
            Console.WriteLine("8 B ASSERT");            
        }

        [TestCase("i")]
        [TestCase("ii")]
        public void even_for_multiple_tests(string numeral)
        {
            Console.WriteLine("8 B {0} ASSERT", numeral);
        }

        class WriteToConsole : TestStep
        {
            public override void BeforeSetUp()
            {
                Console.WriteLine("1 BEFORE SETUP");
            }

            public override void SetUp()
            {
                Console.WriteLine("2 SETUP");
            }

            public override void AfterSetUp()
            {
                Console.WriteLine("3 AFTERSETUP");
            }

            public override void BeforeExecute()
            {
                Console.WriteLine("5 BEFORE EXECUTE");
            }

            public override void Execute()
            {
                Console.WriteLine("6 EXECUTE");
            }

            public override void AfterExecute()
            {
                Console.WriteLine("7 AFTER EXECUTE");
            }

            public override void BeforeAssert()
            {
                Console.WriteLine("8 A BEFORE ASSERT");
            }

            public override void AfterAssert()
            {
                Console.WriteLine("8 C AFTER ASSERT");
            }

            public override void BeforeTearDown()
            {
                Console.WriteLine("9 BEFORE TEARDOWN");
            }

            public override void TearDown()
            {
                Console.WriteLine("10 TEARDOWN");
            }

            public override void AfterTearDown()
            {
                Console.WriteLine("11 AFTER TEARDOWN");
            }
        }
    }
}
