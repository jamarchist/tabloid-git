using System;
using System.Collections.Generic;

namespace Tabloid.xUnit
{
    public class ScenarioTestFixture : IDisposable
    {
        public ScenarioTestFixture()
        {
            Resources = new Dictionary<string, object>();
        }

        public void Dispose()
        {
            Scenario.BeforeTearDown();
            Scenario.TearDown();
            Scenario.AfterTearDown();
        }

        public void ComposeScenario(ITestStep[] allSteps)
        {
            Scenario = new CompositeTestStep(allSteps);
            IsInitialized = true;
        }

        public IDictionary<string, object> Resources { get; private set; }
        public bool IsInitialized { get; private set; }
        public ITestStep Scenario { get; private set; }
    }
}