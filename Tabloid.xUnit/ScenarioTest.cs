﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Tabloid.xUnit
{
    public abstract class ScenarioTest : IScenarioTest, IUseFixture<ScenarioTestFixture>, IDisposable
    {
        public abstract void DefineScenario();

        public void AddStep(ITestStep step)
        {
            steps.Add(step);
        }

        public IDictionary<string, object> Resources
        {
            get { return fixtureData.Resources; }
        }

        public void SetFixture(ScenarioTestFixture data)
        {
            fixtureData = data;
            if (!data.IsInitialized)
            {
                DefineScenario();

                data.ComposeScenario(steps.ToArray());
                data.Scenario.BeforeSetUp();
                data.Scenario.SetUp();
                data.Scenario.AfterSetUp();
            }

            data.Scenario.BeforeAssert();
        }

        public void Dispose()
        {
            fixtureData.Scenario.AfterAssert();
        }

        private ScenarioTestFixture fixtureData;
        private readonly IList<ITestStep> steps = new List<ITestStep>();
    }
}
