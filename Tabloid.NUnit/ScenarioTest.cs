using System.Collections.Generic;
using NUnit.Framework;

namespace Tabloid.NUnit
{
    public abstract class ScenarioTest : IScenarioTest
    {
        private readonly IDictionary<string, object> resources = new Dictionary<string, object>(); 
        private readonly List<ITestStep> steps = new List<ITestStep>();
        private ITestStep scenario;   
     
        [TestFixtureSetUp] // test fixture object
        public void SetUpScenario()
        {
            DefineScenario();

            scenario = new CompositeTestStep(steps.ToArray()) { Test = this };

            // Arranging
            scenario.BeforeSetUp();
            scenario.SetUp();
            scenario.AfterSetUp();

            // Acting
            scenario.BeforeExecute();
            scenario.Execute();
            scenario.AfterExecute();

            // Asserting occurs in the [Test] methods
        }

        [SetUp] // setdata
        public void SetUpTest()
        {
            scenario.BeforeAssert();
        }

        [TearDown] // idisposable
        public void TearDownTest()
        {
            scenario.AfterAssert();
        }

        [TestFixtureTearDown] // test fixture disposable
        public void TearDownScenario()
        {
            scenario.BeforeTearDown();
            scenario.TearDown();
            scenario.AfterTearDown();
        }

        public abstract void DefineScenario();
        protected void AddStep(ITestStep step)
        {
            steps.Add(step);
        }

        public IDictionary<string, object> Resources
        {
            get { return resources; }
        }
    }
}